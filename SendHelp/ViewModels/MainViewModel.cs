﻿using Prism.Commands;
using SendHelp.Core.Contracts.Services;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ICommand = System.Windows.Input.ICommand;

namespace SendHelp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private readonly ICalculationService _calculationService;
        private DelegateCommand _animateBullet;
        private double _pnts;
        private List<double[]> _points;
        private double _randomPos;
        private DelegateCommand _resetCommand;
        private double _velocityY;

        public MainViewModel(ICalculationService calculationService)
        {
            _calculationService = calculationService;
            var r = new Random();
            RandomPos = r.Next(485);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AnimateBullet => _animateBullet ??= new DelegateCommand(Animate);

        public double Pnts
        {
            get => _pnts;
            set
            {
                _pnts = value;
                OnPropertyChanged();
            }
        }

        public List<double[]> Points
        {
            get => _points;
            private set
            {
                _points = value;
                OnPropertyChanged();
            }
        }

        public double RandomPos
        {
            get => _randomPos;
            private set
            {
                _randomPos = value;
                OnPropertyChanged();
            }
        }

        public ICommand ResetCommand => _resetCommand ??= new DelegateCommand(Reset);

        public double VelocityY
        {
            get => _velocityY;
            set
            {
                _velocityY = value;
                OnPropertyChanged();
            }
        }

        public void AutomaticSwitch()
        {
            Thread.Sleep(7000);
            var r = new Random();
            RandomPos = r.Next(75, 485);
        }

        public void isHit(bool isHit) //TODO: Randomize the player2 position when hit
        {
            if (isHit == true)
            {
                Pnts = Pnts + 1;
            }
            else
            {
            }
        }

        private void Animate()
        {
            Points = _calculationService.CalculatePoints(VelocityY);
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Reset()
        {
            var r = new Random();
            RandomPos = r.Next(75, 485);
        }
    }
}