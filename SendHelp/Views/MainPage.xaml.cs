﻿using SendHelp.Core.Contracts.Services;
using SendHelp.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace SendHelp.Views;

public partial class MainPage
{
    private const int TotalAnimationDuration = 10;
    private readonly ICalculationService _calculationService;

    public MainPage(ICalculationService calculationService)
    {
        _calculationService = calculationService;
        InitializeComponent();
    }

    private void AnimateBullet()
    {
        if (DataContext is not MainViewModel vm)
        {
            return;
        }

        var rawPoints = _calculationService.CalculatePoints(vm.VelocityY);

        var points = rawPoints.Select(vmPoint => new Point(vmPoint[0], vmPoint[1])).ToList();

        PathGeometry pathGeometry = new();
        PathFigure pathFigure = new()
        {
            StartPoint = points[0]
        };

        PolyLineSegment polyLineSegment = new();

        //ReSharper disable all
        var boxWidth = 50;
        var boxHeight = 25;

        foreach (var point in points.Skip(1))
        {
            var pointPlayer2 = Player2.TranslatePoint(new Point(0, 0), Canvas1);

            if (point.X >= pointPlayer2.X && point.X <= (pointPlayer2.X + boxWidth) && point.Y >= pointPlayer2.Y &&
                point.Y <= (pointPlayer2.Y + boxHeight))
            {
                AnimateHit();
                return;
            }

            polyLineSegment.Points.Add(point);
        }

        pathFigure.Segments.Add(polyLineSegment);
        pathGeometry.Figures.Add(pathFigure);

        DoubleAnimationUsingPath animationX = new()
        {
            PathGeometry = pathGeometry,
            Source = PathAnimationSource.X,
            Duration = TimeSpan.FromSeconds(TotalAnimationDuration)
        };

        DoubleAnimationUsingPath animationY = new()
        {
            PathGeometry = pathGeometry,
            Source = PathAnimationSource.Y,
            Duration = TimeSpan.FromSeconds(TotalAnimationDuration)
        };

        Storyboard.SetTarget(animationX, Bullet);
        Storyboard.SetTargetProperty(animationX, new PropertyPath(Canvas.LeftProperty));

        Storyboard.SetTarget(animationY, Bullet);
        Storyboard.SetTargetProperty(animationY, new PropertyPath(Canvas.TopProperty));

        Storyboard storyboard = new();
        storyboard.Children.Add(animationX);
        storyboard.Children.Add(animationY);

        storyboard.Begin();
    }

    private void AnimateHit()
    {
        if (DataContext is not MainViewModel vm)
        {
            return;
        }

        var rawPoints = _calculationService.CalculatePoints(vm.VelocityY);

        var points = rawPoints.Select(vmPoint => new Point(vmPoint[0], vmPoint[1])).ToList();

        PathGeometry pathGeometry = new();
        PathFigure pathFigure = new()
        {
            StartPoint = points[0]
        };

        PolyLineSegment polyLineSegment = new();

        //ReSharper disable all
        var boxWidth = 50;
        var boxHeight = 25;

        bool isHit = true;

        foreach (var point in points.Skip(1))
        {
            polyLineSegment.Points.Add(point);
        }

        pathFigure.Segments.Add(polyLineSegment);
        pathGeometry.Figures.Add(pathFigure);

        DoubleAnimationUsingPath animationX = new()
        {
            PathGeometry = pathGeometry,
            Source = PathAnimationSource.X,
            Duration = TimeSpan.FromSeconds(TotalAnimationDuration)
        };

        DoubleAnimationUsingPath animationY = new()
        {
            PathGeometry = pathGeometry,
            Source = PathAnimationSource.Y,
            Duration = TimeSpan.FromSeconds(TotalAnimationDuration)
        };

        Storyboard.SetTarget(animationX, Bullet);
        Storyboard.SetTargetProperty(animationX, new PropertyPath(Canvas.LeftProperty));

        Storyboard.SetTarget(animationY, Bullet);
        Storyboard.SetTargetProperty(animationY, new PropertyPath(Canvas.TopProperty));

        Storyboard storyboard = new();
        storyboard.Children.Add(animationX);
        storyboard.Children.Add(animationY);

        vm.isHit(isHit);
        storyboard.Begin();
        vm.AutomaticSwitch();
    }

    private void Visualize_OnClick(object sender, RoutedEventArgs e)
    {
        AnimateBullet();
    }
}