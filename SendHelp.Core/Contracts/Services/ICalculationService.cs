﻿namespace SendHelp.Core.Contracts.Services;

public interface ICalculationService
{
    List<double[]> CalculatePoints(double velocityY);
}