﻿using SendHelp.Core.Contracts.Services;

// ReSharper disable ClassNeverInstantiated.Global

namespace SendHelp.Core.Models
{
    public class CalculationService : ICalculationService
    {
        private const double Gravity = 1;
        private const double InitialX = 50;
        private const double InitialY = 500;
        private const double VelocityX = 20;

        public List<double[]> CalculatePoints(double velocityY)
        {
            var points = new List<double[]>();
            for (var i = .0; i < 100; i += .1)
            {
                var newX = InitialX + VelocityX * i;
                var newY = InitialY + -1 * velocityY * i + 0.5 * Gravity * Math.Pow(i, 2);
                if (newY < 0)
                {
                    break;
                }
                points.Add([newX, newY]);
            }

            return points;
        }
    }
}